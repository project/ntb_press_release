<?php

namespace Drupal\ntb_press_release;

/**
 * Class NtbResponse.
 */
class NtbResponse {

  /**
   * A list of releases in the current response.
   *
   * @var array
   */
  protected $releases;

  /**
   * The total count of releases.
   *
   * @var int
   */
  protected $totalCount;

  /**
   * The property nextPage.
   *
   * @var int|null
   */
  protected $nextPage;

  /**
   * Get total count.
   *
   * @return int
   *   Number.
   */
  public function getTotalCount(): int {
    return $this->totalCount;
  }

  /**
   * Get next page.
   *
   * @return int|null
   *   Either the next page number, or null if none.
   */
  public function getNextPage() {
    return $this->nextPage;
  }

  /**
   * Does the response have a next page?
   *
   * @return bool
   *   Well does it?
   */
  public function hasNextPage() {
    return NULL !== $this->nextPage;
  }

  /**
   * Getter.
   *
   * @return array
   *   The releases in the current response.
   */
  public function getReleases(): array {
    return $this->releases;
  }

  /**
   * Setter.
   *
   * @param array $releases
   *   From json.
   */
  public function setReleases(array $releases) {
    $this->releases = $releases;
  }

  /**
   * Helper to create this thing.
   *
   * @param array $json
   *   Raw json.
   */
  public static function fromJson(array $json) {
    $response = new static();
    $response->setReleases($json['releases']);
    $response->totalCount = $json['totalCount'];
    $response->nextPage = $json['nextPage'];
    return $response;
  }

}
