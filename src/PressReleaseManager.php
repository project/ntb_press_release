<?php

namespace Drupal\ntb_press_release;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Site\Settings;

/**
 * PressReleaseManager service.
 */
class PressReleaseManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The site settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a PressReleaseManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Site\Settings $settings
   *   The site settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, Settings $settings, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->settings = $settings;
    $this->configFactory = $config_factory;
    $this->config = $config_factory->get('ntb_press_release.settings');
  }

  /**
   * Sync values.
   */
  public function syncEntityValues(EntityInterface $entity, array $release) {
    $entity_type = $this->getEntityType();
    $def = $this->entityTypeManager->getDefinition($entity_type);
    $values = $this->getValuesFromRelease($release);
    // Now, no use in syncing the bundle key.
    $keys = $def->getKeys();

    if (!empty($keys['bundle']) && !empty($values[$keys['bundle']])) {
      unset($values[$keys['bundle']]);
    }
    foreach ($values as $key => $value) {
      $entity->set($key, $value);
    }
    $entity->save();
  }

  /**
   * Helper.
   */
  protected function getEntityType() {
    $entity_type = $this->config->get('entity_type');
    if (empty($entity_type)) {
      throw new \Exception('No entity type specified for NTB settings');
    }
    return $entity_type;
  }

  /**
   * Given a press release, create an entity.
   */
  public function createEntityByRelease($release) {
    $entity_type = $this->getEntityType();
    $storage = $this->entityTypeManager->getStorage($entity_type);
    $values = $this->getValuesFromRelease($release);
    $entity = $storage->create($values);
    $entity->save();
    return $entity;
  }

  /**
   * Release values.
   */
  public function getValuesFromRelease(array $release) {
    $entity_type = $this->config->get('entity_type');
    $def = $this->entityTypeManager->getDefinition($entity_type);
    $values = [
      'ntb_release_id' => $release['id'],
      $def->getKey('label') => $release['title'],
    ];
    $bundle = $this->config->get('bundle');
    if (!empty($bundle)) {
      $bundle_key = $def->getKey('bundle');
      $values[$bundle_key] = $bundle;
    }
    $values['created'] = strtotime($release["published"]);
    $this->moduleHandler->alter('ntb_press_release_entity_create_values', $values, $release);
    return $values;
  }

  /**
   * Method description.
   */
  public function getEntityByReleaseId($id) {
    $entity_type = $this->config->get('entity_type');
    if (empty($entity_type)) {
      throw new \Exception('No entity type specified for NTB settings');
    }
    $storage = $this->entityTypeManager->getStorage($entity_type);
    $ids = $storage
      ->getQuery()
      ->condition('ntb_release_id', $id)
      ->execute();
    if (empty($ids)) {
      return FALSE;
    }
    if (count($ids) > 1) {
      throw new \Exception('More than one release found with id ' . $id);
    }
    $nid = reset($ids);
    return $storage->load($nid);
  }

}
