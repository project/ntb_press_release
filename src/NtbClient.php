<?php

namespace Drupal\ntb_press_release;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Site\Settings;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * NtbClient service.
 */
class NtbClient {

  const BASE_URL = 'https://kommunikasjon.ntb.no/json/v2/';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a NtbClient object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   */
  public function __construct(ClientInterface $http_client, LoggerInterface $logger, ConfigFactoryInterface $configFactory, ModuleHandlerInterface $moduleHandler) {
    $this->httpClient = $http_client;
    $this->logger = $logger;
    $this->config = $configFactory->get('ntb_press_release.settings');
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Gets all releases for a publisher.
   *
   * @return \Drupal\ntb_press_release\NtbResponse
   *   The NTB response if all goes well.
   */
  public function getReleases() {
    $id = $this->getPublisher();
    if (empty($id)) {
      throw new \InvalidArgumentException('No publisher ID specified');
    }
    $channels = $this->getChannels();
    if (!empty($channels)) {
      // Make sure it is an array.
      if (!is_array($channels)) {
        throw new \InvalidArgumentException('Channels setting must be an array');
      }
    }
    // Now build the URL.
    $url = sprintf('%s/releases', self::BASE_URL);
    $response = $this->httpClient->request('GET', $url, [
      'query' => [
        'publisher' => $id,
        'channels' => $channels ? implode(',', $channels) : '',
      ],
    ]);
    $body = (string) $response->getBody();
    $json = @Json::decode($body);
    if (empty($json)) {
      throw new \Exception('No JSON in the response body');
    }
    return NtbResponse::fromJson($json);
  }

  /**
   * Gets the publisher ID.
   */
  protected function getPublisher() {
    return $this->getSetting('publisher');
  }

  /**
   * Gets the channels to use.
   */
  protected function getChannels() {
    return $this->getSetting('channels');
  }

  /**
   * Helper.
   */
  protected function getSetting($setting_name) {
    $value = $this->config->get($setting_name);
    // Also allow it to be set in settings.php.
    $value = Settings::get('ntb_press_release_' . $setting_name, $value);
    // And allow module to set it either way.
    $this->moduleHandler->alter('ntb_press_release_' . $setting_name, $value);
    return $value;
  }

}
