<?php

namespace Drupal\ntb_press_release\Commands;

use Drupal\ntb_press_release\NtbClient;
use Drupal\ntb_press_release\PressReleaseManager;
use Drush\Commands\DrushCommands;
use Psr\Log\LoggerInterface;

/**
 * A Drush commandfile.
 */
class NtbPressReleaseCommands extends DrushCommands {

  /**
   * Client.
   *
   * @var \Drupal\ntb_press_release\NtbClient
   */
  protected $client;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $drupalLogger;

  /**
   * Release manager.
   *
   * @var \Drupal\ntb_press_release\PressReleaseManager
   */
  protected $releaseManager;

  /**
   * NtbPressReleaseCommands constructor.
   */
  public function __construct(NtbClient $client, LoggerInterface $logger, PressReleaseManager $pressReleaseManager) {
    parent::__construct();
    $this->client = $client;
    $this->drupalLogger = $logger;
    $this->releaseManager = $pressReleaseManager;
  }

  /**
   * Sync all press releases.
   *
   * @command ntb_press_release:sync-all
   * @aliases ntb:sa
   */
  public function pressReleaseSync() {
    try {
      $data = $this->client->getReleases();
      foreach ($data->getReleases() as $release) {
        if (empty($release['id'])) {
          // Nothing to do here.
          // todo: Log this?
          continue;
        }
        // Now lets see if we already have this.
        try {
          $entity = $this->releaseManager->getEntityByReleaseId($release['id']);
          if (!$entity) {
            $entity = $this->releaseManager->createEntityByRelease($release);
          }
          $this->releaseManager->syncEntityValues($entity, $release);
        }
        catch (\Throwable $e) {
          $this->drupalLogger->error('Caught exception trying to sync id @id. Error was @err and trace was @trace', [
            '@id' => $release['id'],
            '@err' => $e->getMessage(),
            '@trace' => $e->getTraceAsString(),
          ]);
        }
      }
    }
    catch (\Throwable $e) {
      $this->drupalLogger->error('Caught an exception trying to sync all releases. The error was @err and the stack was @stack', [
        '@err' => $e->getMessage(),
        '@stack' => $e->getTraceAsString(),
      ]);
      $this->logger()->error('There was an error trying to sync all releases');
    }
  }

}
